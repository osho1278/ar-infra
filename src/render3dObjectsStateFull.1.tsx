"use strict";

import React, { Component } from "react";
import {
  StyleSheet
  // TouchableHighlight
} from "react-native";
import {
  ViroNode,
  ViroFlexView,
  Viro3DObject,
  ViroARScene,
  ViroARImageMarker,
  ViroText,
  ViroImage,
  ViroVideo,
  ViroAnimatedImage,
  ViroScene
} from "react-viro";

interface Props {
  objects?: any;
  scale?: any[];
  rotation?: any[];
}
interface State {}
export class Render3DObjects extends React.Component<Props, State> {
  //  props=props;
  constructor(props) {
    super(props);
    console.log(props);
  }
  render() {
    console.log("Got the following at Render3DObjects class", this.props);
    return this.props.objects.map((item: any, index: number) => (
      // console.log(item.objUri,' done !');

      <ViroARImageMarker
        target={`${item.target}`}
        onAnchorFound={() => {
          console.log("Anchor has been found");
        }}
      >
        <Viro3DObject
          animation={{
            name: "rotate",
            delay: 250,
            loop: true,
            onStart: () => {
              console.log("Started animation");
            },
            onFinish: () => {
              console.log("Started animation");
            },
            run: true,
            duration: 60000
          }}
          source={{
            uri: item.objUri
          }}
          resources={
            {uri: item.mtlUri}
          }
          highAccuracyEvents={false}
          position={[0, 0, 0]}
          scale={[0.000005, 0.000005, 0.000005]}
          rotation={this.props.rotation}
          type="OBJ"
          dragType={"FixedToPlane"}
          onDrag={() => {}}
          transformBehaviors={["billboard"]}
          // onPinch={(pinchState, scaleFactor, source) => {
          //   let newScale = this.state.scale.map(item => item * scaleFactor);
          //   if (pinchState == 3) {
          //     // update scale of obj by multiplying by scaleFactor  when pinch ends.
          //     console.log("Pinch has stooped");
          //     // let newScale = this.state.scale.map(item => item * scaleFactor);
          //     this.setState({ scale: newScale }); // newScale

          //     return;
          //   }
          // }}
          // onRotate={(rotateState, rotationFactor, source) => {
          //   let newRotation = this.state.scale.map(
          //     item => item - rotationFactor
          //   );
          //   if (rotateState == 3) {
          //     console.log("Rotation has ended");

          //     this.setState({ rotation: newRotation }); // newScale;
          //     return;
          //   }
          //   //update rotation using setNativeProps
          // }}
          onClick={(position, source) => {
            console.log("position", position);
            console.log("source", source);
            // user has clicked the object
          }}
          onLoadStart={() => {
            console.log("Loading has started");
          }}
        />
      
        <ViroFlexView
          rotation={[-90, 0, 0]}
          position={[0.1, 0, 0]}
          //height={0.03}
          // width={0.05}
          style={{ flexDirection: "row", padding: 0.1 }}
        >
       
          <ViroFlexView style={styles.cardWrapper}>
          <ViroVideo
          source={{uri:"http://192.168.1.7:5005/3d/videos/ShashiTharoor_2009I-480p-en.mp4"}}
          loop={true}
          position={[0.15,0.015,0]}
          // rotation={[0, -90, 0]}
          scale={[2, 2, 0]}
          height={0.01}
          width={0.01}
          visible={true}
          dragType={"FixedToPlane"}
          onError={(e)=>{console.log('Errorr',e)}}
          onBufferStart={()=>{console.log('Video has started buffering')}}
          onTouch={()=>{console.log('Video was touched')}}
       />

            <ViroText
              // fontSize={24}
              style={styles.boldFont}
              position={[0.015, 0, 0]}
              width={1}
              rotation={[0, -5, 0]}
              // height={0.0001}
              extrusionDepth={2}
              materials={["frontMaterial", "backMaterial", "sideMaterial"]}
              text={`${item.target}`}
            />
            
          </ViroFlexView>
         
        </ViroFlexView>
      </ViroARImageMarker>
    )); // definition for map ends // retrun ends
  } // render ends
}

var styles = StyleSheet.create({
  textStyle: {
    backgroundColor: "red",
    flex: 1,
    fontFamily: "Roboto",
    fontSize: 5,
    color: "blue",
    textAlignVertical: "top",
    textAlign: "left",
    fontWeight: "bold"
  },
  card: {
    flex: 8,
    flexDirection: "column"
  },
  cardWrapper: {
    flexDirection: "row",
    alignItems: "flex-start",
    padding: 0.001,
    flex: 0.5
  },
  subText: {
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flex: 0.5
  },
  boldFont: {
    color: "#FFFFFF",
    flex: 0.5,
    textAlignVertical: "center",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 2
  }
});
