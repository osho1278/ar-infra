// import { AppRegistry } from 'react-native';
import React, { Component } from 'react';

import App from './App';
// import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './reducers'
// import configureStore from './store';

// const store = configureStore()
const store = createStore(rootReducer)
export default class Main extends React.Component{
render()
    {return(
        
    <Provider store={store}>
      <App />
    </Provider>
  
    )}
}