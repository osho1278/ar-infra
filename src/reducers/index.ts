import { combineReducers } from 'redux';

const INITIAL_STATE = {
    id: '',
    text: '',
    position: [.3, .3, .1],
    scale: [.09, .09, 1]
};

const markerReducer = (state = [], action:any):any => {
    switch (action.type) {
        case "ADD_POLYGON":
            console.log(state,action)
            return state.some((item:any)=>item.id===`POLYGON_${action.payload}`) ?
            [...state] : 

            [...state,
                    {
                        id: `POLYGON_${action.payload}`,
                        text: '',
                        position: [0, .5, -1],
                        scale: [.29, .29, 1]
                    }
                ]
        case "ADD_TEXT_TO_MARKER":
        console.log('ADD_TEXT_TO_MARKER')
        return [...state.map((item:any)=>{
            return item.id===`${action.payload.id}` ? 
            action.payload
            :
            item
        })]
        
        case "DRAG_MARKER":
        console.log('DRAG_MARKER')
        return [...state.map((item:any)=>{
            return item.id===`${action.payload.id}` ? 
            action.payload
            :
            item
        })]
        default:
            return state
    }
};
const addTextReducer = (state = [], action:any):any => {
    switch (action.type) {
        case "ADD_TEXT":
        console.log('Adding Text');
        return [action.payload]
        case "DONE_ADD_TEXT":
        console.log('DONE_ADD_TEXT');
        return []
        default:
            return state
    }
};
export default combineReducers({
    markers: markerReducer,
    addText:addTextReducer
});