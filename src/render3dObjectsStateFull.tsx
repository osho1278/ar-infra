"use strict";

import React, { Component } from "react";
import {
  StyleSheet
  // TouchableHighlight
} from "react-native";
import {

  Viro3DObject,
  ViroARScene,
  ViroARImageMarker,

} from "react-viro";

interface Props{
objects?:any;
scale?:any[];
rotation?:any[];
}
interface State{

}
export class  Render3DObjects extends React.Component<Props,State>{
    
    //  props=props;
    constructor(props){
        super(props);
        console.log(props);
    }
   render(){
       console.log('Got the following',this.props);
        return(
           
              this.props.objects.map((item:any,index:number)=>

            // console.log(item.objUri,' done !');
            
            <ViroARImageMarker target={`${item.target}`}
            onAnchorFound={()=>{
              console.log('Anchor has been found');
            }}
            
            >



            
              <Viro3DObject
                animation={{
                  name: "rotate",
                  delay: 250,
                  loop: true,
                  onStart: () => {
                    console.log("Started animation");
                  },
                  onFinish: () => {
                    console.log("Started animation");
                  },
                  run: true,
                  duration: 60000
                }}
                source={{
                  uri:
                    item.objUri
                }}
               
              resources={{
                  uri:
                    `${item.mtlUri}`
                }}
                highAccuracyEvents={false}
                
                position={[0, 0, 0]}   
                scale={[0.000005,0.000005,0.000005]}
                rotation={this.props.rotation}
                type="OBJ"
                dragType={"FixedToPlane"}
                onDrag={() => {}}
                transformBehaviors={["billboard"]}
                // onPinch={(pinchState, scaleFactor, source) => {
                //   let newScale = this.state.scale.map(item => item * scaleFactor);
                //   if (pinchState == 3) {
                //     // update scale of obj by multiplying by scaleFactor  when pinch ends.
                //     console.log("Pinch has stooped");
                //     // let newScale = this.state.scale.map(item => item * scaleFactor);
                //     this.setState({ scale: newScale }); // newScale
    
                //     return;
                //   }
                // }}
                // onRotate={(rotateState, rotationFactor, source) => {
                //   let newRotation = this.state.scale.map(
                //     item => item - rotationFactor
                //   );
                //   if (rotateState == 3) {
                //     console.log("Rotation has ended");
                   
                //     this.setState({ rotation: newRotation }); // newScale;
                //     return;
                //   }
                //   //update rotation using setNativeProps
                // }}
                onClick={(position, source) => {
                  console.log("position", position);
                  console.log("source", source);
                  // user has clicked the object
                }}
                onLoadStart={() => {
                  console.log("Loading has started");
                }}
              />
            </ViroARImageMarker>

              ) // map ends
           
        ) // retrun
            }
      
    }
