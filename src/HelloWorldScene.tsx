'use strict';

import React, { Component } from 'react';

// import {ConView,Button,Text} from 'native-base';
import {
  StyleSheet,
  // TouchableHighlight
} from 'react-native';

import { Container, Header, Content, Button, Text } from 'native-base';

import {
  ViroScene,
  ViroText,
  Viro360Image,
  ViroARSceneNavigator,
  ViroButton
} from 'react-viro';

export default class HelloWorldScene extends Component {


  render() {
    return (
      <Container>
        <Header />
        <Content>
          <Button>
            <Text>Click Me!</Text>
          </Button>
      <ViroScene>
        <Viro360Image source={require('./res/others/guadalupe_360.jpg')} />
        <ViroText text="Hello World!" width={2} height={2} position={[0, 0, -2]} style={styles.helloWorldTextStyle} />
    
      </ViroScene>
        </Content>
      </Container>
     
    );
  }
  _onButtonGaze() {
    this.setState({
        buttonStateTag: "onGaze"
    });
}
_onButtonTap() {
    this.setState({
        buttonStateTag: "onTap"
    });
}
}
var styles = StyleSheet.create({
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 60,
    color: '#ffffff',
    textAlignVertical: 'center',
    textAlign: 'center',  
  },
});

// module.exports = HelloWorldScene;
