export const addPolygon = (polygonIndex:any) => ({
    type: 'ADD_POLYGON',
    payload: polygonIndex,
});

export const addText = (polygonIndex:any) => ({
    type: 'ADD_TEXT',
    payload: polygonIndex,
});

export const doneAddText = (polygonIndex:any) => ({
    type: 'DONE_ADD_TEXT',
    payload: polygonIndex,
});

export const addTextToMarker = (polygonIndex:any) => ({
    type: 'ADD_TEXT_TO_MARKER',
    payload: polygonIndex,
});


export const dragMarker = (polygonIndex:any) => ({
    type: 'DRAG_MARKER',
    payload: polygonIndex,
});
