/**
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

import React, { Component } from "react";
import { TouchableOpacity, TextInput } from "react-native";
import ContextMenuButton from './components/ContextMenuButton';
import ButtonComponent from './components/ButtonComponent';
import {
  AppRegistry,
  Text,
  View,
  StyleSheet,
  PixelRatio,
  TouchableHighlight
} from "react-native";

import {
  ViroVRSceneNavigator,
  ViroARSceneNavigator,
  ViroBox,
  ViroMaterials
} from "react-viro";
import { Container, Header, Content, Button } from "native-base";
// import { Polygon } from './src/polygon';
import { bindActionCreators } from "redux";

import { connect } from "react-redux";
import { addPolygon, doneAddText, addTextToMarker } from "./actions";
/*
 TODO: Insert your API key below
 */

var sharedProps = {
  apiKey: "D13B2199-E941-4D1C-A015-4863E1E85348"
  // shouldAddPolygon:false
};

// Sets the default scene you want for AR and VR

import InitialARScene from "./HelloWorldSceneAR";
// var InitialARScene = require('./js/HelloWorldSceneAR');
var InitialVRScene = require("./HelloWorldScene");
// var polygon=require('./js/polygon');
var UNSET = "UNSET";
var VR_NAVIGATOR_TYPE = "VR";
var AR_NAVIGATOR_TYPE = "AR";
var Polygon_Count = 0;

// This determines which type of experience to launch in, or UNSET, if the user should
// be presented with a choice of AR or VR. By default, we offer the user a choice.
var defaultNavigatorType = UNSET;
export interface Props {
  markers: any;
  addTextToMarker: Function;
  doneAddText: Function;
  addPolygon: Function;
}
export interface State {
  navigatorType: any;
  sharedProps: any;
  shouldAddPolygon: boolean;
  markers: any;
  text: string;
}
export class ViroSample extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      navigatorType: defaultNavigatorType,
      sharedProps: sharedProps,
      shouldAddPolygon: false,
      markers: props.markers,
      text: ""
    };
    this._getExperienceSelector = this._getExperienceSelector.bind(this);
    this._getARNavigator = this._getARNavigator.bind(this);
    this._getVRNavigator = this._getVRNavigator.bind(this);
    this._getExperienceButtonOnPress = this._getExperienceButtonOnPress.bind(
      this
    );
    this._exitViro = this._exitViro.bind(this);
  }

  // Replace this function with the contents of _getVRNavigator() or _getARNavigator()
  // if you are building a specific type of experience.
  render() {
    if (this.state.navigatorType == UNSET) {
      return this._getExperienceSelector();
    } else if (this.state.navigatorType == VR_NAVIGATOR_TYPE) {
      return this._getVRNavigator();
    } else if (this.state.navigatorType == AR_NAVIGATOR_TYPE) {
      return this._getARNavigator(this.props);
    }
    return this._getARNavigator(this.props);
  }

  // Presents the user with a choice of an AR or VR experience
  _getExperienceSelector() {
    console.log(this.props);
    return (
      <View style={localStyles.outer}>
        <View style={localStyles.inner}>
          <Text style={localStyles.titleText}>
            Choose your desired experience:
          </Text>

          <TouchableHighlight
            style={localStyles.buttons}
            onPress={this._getExperienceButtonOnPress(AR_NAVIGATOR_TYPE)}
            underlayColor={"#68a0ff"}
          >
            <Text style={localStyles.buttonText}>AR</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={localStyles.buttons}
            onPress={this._getExperienceButtonOnPress(VR_NAVIGATOR_TYPE)}
            underlayColor={"#68a0ff"}
          >
            <Text style={localStyles.buttonText}>VR</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }

  // Returns the ViroARSceneNavigator which will start the AR experience
  _getARNavigator(props: any) {
    console.log("App.js has props", props);
    return (
      <View style={{ flex: 1 }}>
        <View style={{ padding: 10, flexDirection: "row" }}>
          <Button
            onPress={this.startRecording}
            //onPress={onPressLearnMore}
            style={{ flex: 1 }}
            color="yellow"
          >
            <Text style={localStyles.buttonText}>Record</Text>
          </Button>
          <Button style={{ flex: 1 }} onPress={this.stopRecording} color="red">
            <Text style={localStyles.buttonText}>Stop Rec</Text>
          </Button>

          <Button style={{ flex: 1 }} onPress={this.renderPolygon} color="red">
            <Text style={localStyles.buttonText}>Polygon</Text>
          </Button>
        </View>
        <View style={{ padding: 10 }}>
          {props.addText && props.addText.length !== 0 && (
            <View style={localStyles.container}>
              <TextInput
                style={localStyles.input}
                underlineColorAndroid="transparent"
                placeholder="Placeholder"
                placeholderTextColor="#9a73ef"
                autoCapitalize="none"
                onChangeText={data => this.setState({ text: data })}
              />

              <TouchableOpacity
                style={localStyles.submitButton}
                onPress={() => {
                  let data = props.addText[0];
                  data.text = this.state.text;
                  this.props.addTextToMarker(data);
                  this.props.doneAddText();
                }}
              >
                <Text style={localStyles.submitButtonText}> Submit </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
        <View style={{ flex: 3 }}>
          <ViroARSceneNavigator
            {...this.state.sharedProps}
            initialScene={{ scene: InitialARScene }}
            ref={"arSceneNavigator"}
            viroAppProps={this.state}
          />
        </View>
          {/* 2D UI buttons on top right of the app, that appear when a 3D object is tapped in the AR Scene */}
          {this._renderContextMenu()}

          {/* This menu contains the buttons on bottom left corner - toggle listview contents between 
          Portals, Effects and Models (objects) */}
          { this._renderButtonLeftMenu()
          }
          {/* 2D UI for sharing rendered after user finishes taking a video / screenshot */}
          {
            //this._renderShareScreen()
          }

          {/* 2D UI rendered to enable the user changing background for Portals using stock images/videos or through their camera roll */}
          {
            //this._renderPhotosSelector()
          }

          {/* Buttons and their behavior for recording videos and screenshots at the bottom of the screen */}
          {
            //this._renderRecord()
          }
      </View>
    );
  }
// Context Menu is the collection of three buttons that appear on the top right with "Remove object (or Portal)", "Clear All" and "Photo Selector (Only for Portals)"
_renderContextMenu() {
  // var selectedItemIndex = this.props.currentItemSelectionIndex;
  // var clickState = this.props.currentItemClickState;
  var totalHeight = 120;
  // if(this.props.currentSelectedItemType != UIConstants.LIST_MODE_PORTAL) {
  //     totalHeight = 80;
  // }
  // if (selectedItemIndex != -1 && clickState == 2) {
    // If a valid object (or portal) was clicked, reset the items "click state" after 3.5 seconds 
    // So that the item can "clicked" again.
  //   TimerMixin.setTimeout(
  //     () => {
  //       this.props.dispatchChangeItemClickState(-1, '', '');
  //     },
  //     3500
  //   );
  // }
    return (
      <View style={{flex:1, position:'absolute', flexDirection:'column', justifyContent: 'space-between', alignItems: 'flex-end', top:'25%', right:10,width:80, height:220}}>
        <View style={{flex:.45, flexDirection:'column', justifyContent: 'space-between',alignItems: 'flex-end', right:0, top:20, width:80}}>
           {
             // renderIf(this.props.currentItemSelectionIndex != -1 && (this.state.showPhotosSelector==false),
            <ContextMenuButton onPress={()=>{console.log('trash pressed')} // this._onContextMenuRemoveButtonPressed
          } 
                    stateImageArray={[require("./res/btn_trash.png")]}
                    style={localStyles.previewScreenButtons} />
        //  )
        }

          {
            //renderIf(this.props.currentItemSelectionIndex != -1 && (this.state.showPhotosSelector==false),
            <ContextMenuButton onPress={
              ()=>{console.log("clear pressed")}
            //  this._onContextClearAll
            }
                    stateImageArray={[require("./res/btn_clear_all.png")]}
                    style={localStyles.previewScreenButtons} />
        //  )
        }

        </View>
        <View style={{flex:.2, flexDirection:'column', justifyContent: 'flex-end',alignItems: 'flex-end', width:80}}>
          {
            // renderIf(this.props.currentItemSelectionIndex != -1 && (this.props.currentSelectedItemType == // UIConstants.LIST_MODE_PORTAL) && (this.state.showPhotosSelector==false),
            <ContextMenuButton onPress={()=>{
              console.log("pic v2 pressed")
              // this.setState({showPhotosSelector:true, lastSelectedPortalUUID:this.props.currentItemSelectionIndex})
            
            }} 
                    stateImageArray={[require("./res/btn_add_pic_v2.png")]}
                    style={localStyles.previewScreenButtonsAddPic} />
        //  )
        }
        </View>
      </View>

  );
}



//// 

_renderButtonLeftMenu=()=> {
  var buttons = [];
  // Portal mode button
  buttons.push(
      <ButtonComponent key="button_portals"
        onPress={()=>{
          console.log("btn_mode_portals_on pressed");
          // this.props.dispatchSwitchListMode(UIConstants.LIST_MODE_PORTAL, UIConstants.LIST_TITLE_PORTALS)
        
        }}
        buttonState={
          "on"
        //   (this.props.listMode==UIConstants.LIST_MODE_PORTAL) ? 'on':'off'
        }
        stateImageArray={[require("./res/btn_mode_portals_on.png"), require("./res/btn_mode_portals.png")]}
        style={localStyles.screenIcon} 
        selected={true
          //(this.props.listMode == UIConstants.LIST_MODE_PORTAL)
        }
        />);

  // Effect mode button
  buttons.push(
      <ButtonComponent key="button_effects"
        onPress={()=>{
          console.log("btn_mode_effects pressed");
          //  this.props.dispatchSwitchListMode(UIConstants.LIST_MODE_EFFECT, UIConstants.LIST_TITLE_EFFECTS)
        }}
        buttonState={ "on"
       //   (this.props.listMode==UIConstants.LIST_MODE_EFFECT) ? 'on':'off'
        }
        stateImageArray={[require("./res/btn_mode_effects_on.png"), require("./res/btn_mode_effects.png")]}
        style={localStyles.screenIcon}
         selected={true 
          // (this.props.listMode == UIConstants.LIST_MODE_EFFECT)
        }
        />);

  // Objects mode button
  buttons.push(
      <ButtonComponent key="button_models"
          onPress={()=>{
            console.log('btn_mode_objects')
            // this.props.dispatchSwitchListMode(UIConstants.LIST_MODE_MODEL, UIConstants.LIST_TITLE_MODELS)
          }}
          buttonState={
            "on"
           //  (this.props.listMode==UIConstants.LIST_MODE_MODEL) ? 'on':'off'
          }
          stateImageArray={[require("./res/btn_mode_objects_on.png"), require("./res/btn_mode_objects.png")]}
          style={localStyles.screenIcon} 
          selected={true
           //  (this.props.listMode == UIConstants.LIST_MODE_MODEL)
          }
          />);

  // Show these buttons only if we are in main screen or while recording -> Buttons not rendered when in share screen or when manipulating individual portals
  // if(this.props.currentScreen == UIConstants.SHOW_MAIN_SCREEN || this.props.currentScreen == // UIConstants.SHOW_RECORDING_SCREEN) {
 //   if (this.state.showPhotosSelector==false) {
    return (
         <View style={{position:'absolute', flexDirection:'column', justifyContent: 'space-around',left:10, bottom:70, width:70, height:160, flex:1}}>
            {buttons}
         </View>
      );
  //  }
 // }
 //  return null;
}


//// 




  startRecording = () => {
    console.log("Starting recording", this.refs);
    // @ts-ignore
    this.refs.arSceneNavigator._startVideoRecording(
      new Date(),
      true,
      (err: any) => {
        console.log(err);
      }
    );
  };
  stopRecording = () => {
    console.log("Stopping recording", this.refs);
    // @ts-ignore

    this.refs.arSceneNavigator._stopVideoRecording();
  };

  renderPolygon = () => {
    console.log("adding a ploygon");
    this.setState({ shouldAddPolygon: true, markers: this.props.markers });
    this.props.addPolygon(++Polygon_Count);
    //this.refs.arSceneNavigator.viroAppProps({shouldAddPolygon:true});
  };

  // Returns the ViroSceneNavigator which will start the VR experience
  _getVRNavigator() {
    return (
      <ViroVRSceneNavigator
        {...this.state.sharedProps}
        initialScene={{ scene: InitialVRScene }}
        onExitViro={this._exitViro}
      />
    );
  }

  // This function returns an anonymous/lambda function to be used
  // by the experience selector buttons
  _getExperienceButtonOnPress(navigatorType: any) {
    return () => {
      this.setState({
        navigatorType: navigatorType
      });
    };
  }

  // This function "exits" Viro by setting the navigatorType to UNSET.
  _exitViro() {
    this.setState({
      navigatorType: UNSET
    });
  }
}

var localStyles = StyleSheet.create({
  container: {
    paddingTop: 0
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: "#7a42f4",
    borderWidth: 1,
    color: "green"
  },
  submitButton: {
    backgroundColor: "#7a42f4",
    padding: 5,
    margin: 15,
    height: 40
  },
  previewScreenButtons: {
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },

  submitButtonText: {
    color: "white"
  },
  viroContainer: {
    flex: 1,
    backgroundColor: "black"
  },
  outer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "black"
  },
  inner: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "black"
  },
  titleText: {
    paddingTop: 30,
    paddingBottom: 20,
    color: "#fff",
    textAlign: "center",
    fontSize: 25
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 20,
    alignItems: "center"
  },
  buttons: {
    height: 80,
    width: 150,
    paddingTop: 20,
    paddingBottom: 20,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#68a0cf",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff"
  },
  screenIcon: {
    position : 'absolute',
    height: 58,
    width: 58,
  },
  exitButton: {
    height: 50,
    width: 100,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#68a0cf",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#fff"
  },
  previewScreenButtonsAddPic: {
    height: 32,
    width: 37,
  },
});

const mapStateToProps = (state: any) => {
  const { markers, addText } = state;
  return { markers, addText };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      addPolygon: payload => dispatch(addPolygon(payload)),
      doneAddText: payload => dispatch(doneAddText(payload)),
      addTextToMarker: payload => dispatch(addTextToMarker(payload))
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViroSample);
