'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  // TouchableHighlight
} from 'react-native';
import {
  ViroARScene,
  ViroPolygon,
  ViroConstants,
  ViroBox,
  ViroMaterials,
  ViroAnimations,
  ViroARImageMarker,
  ViroARTrackingTargets,
} from 'react-viro';

export default class Polygon extends Component {

  constructor(props:any) {
    super(props);

    // Set initial state here
    this.state = {
      text : "Initializing AR..."
    };

    // bind 'this' to functions
    this._onInitialized = this._onInitialized.bind(this);
  }

  render() {
    return (
  <ViroARScene >

      <ViroPolygon rotation={[-90, 0, 0]}
             position={[0,0,0]}
             vertices={[[-1,0], [0,1], [1,0]]}
             materials={"blue_plane"}/>
        <ViroARImageMarker target={"targetOne"} >
    <ViroBox position={[0, .25, 0]} scale={[.05, .05, .05]} />
  </ViroARImageMarker>
      </ViroARScene>
    
    );
  }
  _onDisplayDialog(){
    console.log('btn pressed, show dialog')
  }


  _onInitialized(state:any, reason:any) {
    if (state == ViroConstants.TRACKING_NORMAL) {
      this.setState({
        text : "Hello World!"
      });
    } else if (state == ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }
}

var styles = StyleSheet.create({
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 30,
    color: '#ffffff',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
});

ViroMaterials.createMaterials({
  grid: {
    diffuseTexture: require('./res/grid_bg.jpg'),
  },
});

ViroAnimations.registerAnimations({
  rotate: {
    properties: {
      rotateY: "+=90"
    },
    duration: 250, //.25 seconds
  },
});

ViroARTrackingTargets.createTargets({
  "targetOne" : {
    source : require('./res/earth.jpg'),
    orientation : "Up",
    physicalWidth : 0.1 // real world width in meters
  },
});