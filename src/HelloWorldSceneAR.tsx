"use strict";

import React, { Component } from "react";
import {
  StyleSheet
  // TouchableHighlight
} from "react-native";
import {
  ViroARScene,
  ViroText,
  ViroConstants,
  ViroBox,
  ViroMaterials,
  Viro3DObject,
  ViroAmbientLight,
  ViroSpotLight,
  ViroARPlaneSelector,
  ViroNode,
  ViroAnimations,
  ViroARImageMarker,
  ViroARTrackingTargets,
  ViroButton,
  Viro360Image,
  ViroImage,
  ViroARObjectMarker,
  ViroFlexView
} from "react-viro";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";

import { connect } from "react-redux";
import { addText, dragMarker } from "./actions";
import { Render3D } from "./render3dObjects";
import { Render3DObjects } from "./render3dObjectsStateFull.1";

export interface Props {
  markers?: any;
  addTextToMarker?: Function;
  doneAddText?: Function;
  addPolygon?: Function;
  addText?(param: any): any;
  arSceneNavigator?: any;
  dragMarker?: Function;
}
interface State {
  text: string;
  shouldAddPolygon: any;
  showText: string;
  textPosition: any[];
  boxPosition: any[]; // [0, -0.5, -1],
  markers: any; //props.markers,
  showP625: boolean;
  scale?: any;
  rotation?: any;
  objects?: any[];
}


// let baseURL="http://192.168.1.7:5005";
let baseURL="http://10.58.34.188:5005";

// const objUri = baseURL+"/P625_AR.obj";
// const mtlUri = baseURL+"/P625_AR.mtl";


const objUri = baseURL+"/3d/tajmahal/tajmahal.obj";
const mtlUri = baseURL+"/P625_AR.mtl";


export class HelloWorldSceneAR extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);
    console.log("props are HelloWorldSceneAR ", props);
    // Set initial state here
    this.state = {
      text: "Initializing AR...",
      shouldAddPolygon: props.arSceneNavigator.viroAppProps.shouldAddPolygon,
      showText: "",
      textPosition: [],
      boxPosition: [0, -0.5, -1],
      markers: props.markers,
      showP625: false,
      scale: [0.0002, 0.0002, 0.0002],
      rotation: [45, 0, 45],
      objects: [
        {
          target: "france",
          objUri: baseURL+"/3d/eiffel/eiffel.obj",
          mtlUri: baseURL+"/3d/eiffel/eiffel.mtl"
        },
        { target: "india", objUri: objUri, mtlUri: mtlUri }
      ]
    };

    // bind 'this' to functions
    this._onInitialized = this._onInitialized.bind(this);
  }

  componentWillReceiveProps = (props: any) => {
    console.log("componentWillReceiveProps", props);
  };
  componentDidMount = () => {
    console.log("componentdidMount", this.props);
    this.setState({
      shouldAddPolygon: this.props.arSceneNavigator.viroAppProps
        .shouldAddPolygon,
      markers: this.props.markers
    });

    ViroARTrackingTargets.createTargets({
      france: {
        source: { uri: baseURL+"/3d/images/France2.jpg" },
        orientation: "Up",
        physicalWidth: 0.15 // real world width in meters
      },

      india: {
        source: { uri: baseURL+"/3d/images/India2.jpg" },
        orientation: "Up",
        physicalWidth: 0.15 // real world width in meters
      }
    });
  };

  onBoxClicked = (item: any, data: any) => {
    console.log(item, data);
    this.setState({
      showText: "this needs servicing",
      textPosition: data
    });
    item.position = data;
    // this.props.addText(item);
    // this.refs.arSceneNavigator.
  };

  onBoxDrag = (item: any, data: any) => {
    console.log("Dragging", item);
    this.setState({ boxPosition: data });
    item.position = data;
    //this.props.dragMarker(item);
  };
  // models= Render3D({objects:[
  //   {
  //     target: "france",
  //     objUri: baseURL+"/3d/eiffel/eiffel.obj",
  //     mtlUri: baseURL+"/3d/eiffel/eiffel.mtl"
  //   },
  //   { target: "india", objUri: objUri, mtlUri: mtlUri }
  // ],scale:[0.002, 0.002, 0.002],
  //   rotation: [45, 0, 45],})

  objects = {
    objects: [
      {
        target: "france",
        objUri: baseURL+"/3d/eiffel/eiffel.obj",
        mtlUri: baseURL+"/3d/eiffel/eiffel.mtl"
      },
      { target: "india", objUri: objUri, mtlUri: mtlUri }
    ]
  };
  render() {
    // console.log(this.props,this.props.arSceneNavigator.viroAppProps.shouldAddPolygon);
    console.log("shouldAddPolygon", this.state.shouldAddPolygon, this.props);
    // console.log('models',this.models);
    let { markers } = this.props;
    return (
      <ViroARScene
        onTrackingUpdated={this._onInitialized}
        onAnchorFound={this.onAnchorFound}
      >
        <ViroAmbientLight color={"#aaaaaa"} />
        <ViroSpotLight
          innerAngle={5}
          outerAngle={90}
          direction={[0, -1, -0.2]}
          position={[0, 3, 1]}
          color="#ffffff"
          castsShadow={true}
        />
        <ViroNode
          position={[0, -1, 0]}
          dragType="FixedToWorld"
          onDrag={() => {}}
        />

        {
          // this.models
        }
        <Render3DObjects
          objects={this.state.objects}
          scale={[0.002, 0.002, 0.002]}
          rotation={[45, 0, 45]}
        />
      </ViroARScene>
    );
  }
  _onDisplayDialog() {
    console.log("btn pressed, show dialog");
  }
  showP625 = () => {
    console.log("showing P625");
    this.setState({ showP625: true });
  };

  onAnchorFound = (data: any) => {
    console.log(data);
  };

  _onInitialized(state: any, reason: any) {
    if (state == ViroConstants.TRACKING_NORMAL) {
      this.setState({
        text: "Hello World!"
      });
    } else if (state == ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }
}

ViroMaterials.createMaterials({
  grid: {
    diffuseTexture: require("./res/others/grid_bg.jpg")
  },

  frontMaterial: {
    diffuseColor: "#FFFFFF"
  },
  backMaterial: {
    diffuseColor: "#FF0000"
  },
  sideMaterial: {
    diffuseColor: "#0000FF"
  }
});

ViroAnimations.registerAnimations({
  rotate: {
    properties: {
      rotateY: "+=360"
    },
    duration: 60000 //.25 seconds
  }
});

// ViroARTrackingTargets.createTargets({
//   targetOne: {
//     source: require("./res/P625/right-1.jpg"),
//     orientation: "Up",
//     physicalWidth: 0.15 // real world width in meters
//   },
//   france: {
//     source: { uri: baseURL+"/3d/images/France2.jpg" },
//     orientation: "Up",
//     physicalWidth: 0.15 // real world width in meters
//   },

//   india: {
//     source: { uri: baseURL+"/3d/images/India2.jpg" },
//     orientation: "Up",
//     physicalWidth: 0.15 // real world width in meters
//   },
//   targetTwo: {
//     source: require("./res/others/scan.arobject"),
//     type: "Object"
//   }
// });

const mapStateToProps = (state: any) => {
  console.log(state);
  let { markers } = state;
  console.log(markers);
  return { markers };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      addText: payload => dispatch(addText(payload)),
      dragMarker: payload => dispatch(dragMarker(payload))
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HelloWorldSceneAR);
