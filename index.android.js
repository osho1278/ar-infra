import { AppRegistry } from 'react-native';
import App from './src/main';

AppRegistry.registerComponent('SeeThrough', () => App);

// The below line is necessary for use with the TestBed App
AppRegistry.registerComponent('SeeThrough', () => App);
